﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = System.Numerics.Vector3;

// Credit goes to http://www.groebelsloot.com/2016/03/13/pathfinding-part-2/  
// and David Gouveia for providing the functions.

public class TraversablePath : MonoBehaviour
{
    public class Graph
    {
        public struct Edge
        {
            public Vector2 a;
            public Vector2 b;
        }
        private List<Vector2> nodes = new List<Vector2>();
        private List<Edge> edges = new List<Edge>();
        private List<Vector2> shortestPath = new List<Vector2>();
        public void AddNode(Vector2 node)
        {
            if (node != null)
                nodes.Add(node);
        }

        public void RemoveNode(Vector2 node)
        {
            if (node != null)
                nodes.Remove(node);
        }

        public void AddEdge(Edge edge)
        {
            if (edge.a != null)
                edges.Add(edge);
        }

        public void RemoveEdge(Edge edge)
        {
            if (edge.a != null)
                edges.Remove(edge);
        }

        public List<Vector2> GetNodes()
        {
            return nodes;
        }
    }
    // Start is called before the first frame update
    [SerializeField] private List<PolygonCollider2D> m_Polygons = new List<PolygonCollider2D>();

    private List<Vector2> m_ConcaveVertices = new List<Vector2>();

    private Graph m_Graph = new Graph();
    //   [SerializeField] private List<Transform> m_TraversablePath = new List<Transform>();
    void Start()
    {

    }

    private void MakeNodes()
    {
        for (int i = 0; i < m_Polygons.Count; i++)
        {
            int vertexIndex = 0;
            Vector2[] currentPolygon = m_Polygons[i].GetPath(i);
            foreach (Vector2 vertex in currentPolygon)
            {
                if (i != 0)
                {
                    if (!IsVertexConcave(vertexIndex, currentPolygon))
                    {
                        m_Graph.AddNode(vertex);
                    }
                }
                else
                {
                    if (IsVertexConcave(vertexIndex, currentPolygon))
                    {
                        m_Graph.AddNode(vertex);
                    }
                }
                // add node if vertex is concave, if not walkable then non-concave vertices will be added as nodes too.

                ++vertexIndex;
            }
        }
    }

    private bool IsVertexConcave(int vertex, Vector2[] vertices)
    {
        var currentVertex = vertices[vertex];
        var nextVertex = vertices[(vertex + 1) % vertices.Length];
        var prevVertex = vertices[vertex == 0 ? vertices.Length - 1 : vertex - 1];

        Vector3 leftEdge = new Vector3(nextVertex.x - currentVertex.x, nextVertex.y - currentVertex.y, 0.0f);
        Vector3 rightEdge = new Vector3(nextVertex.x - currentVertex.x, nextVertex.y - currentVertex.y, 0.0f);

        float cross = Vector3.Cross(leftEdge, rightEdge).Length();

        if (cross > 0.0f || cross < 0.0f)
            return true;
        else return false;
    }

    private bool IsInLineOfSight(Vector2 start, Vector2 end)
    {
        float epsilon = 0.5f;


        // not is LOS if any of the ends is outside the polygon
        if (!m_Polygons[0].OverlapPoint(start) || !m_Polygons[0].OverlapPoint(end))
        {
            return false;
        }

        // in LOS if it's the same start and end location
        if ((start - end).magnitude <= epsilon)
        {
            return false;
        }

        //Not in LOS if any edge is intersected by start-end line
        // bool inSight = true;
        foreach (PolygonCollider2D polygon in m_Polygons)
        {
            int vertexLength = polygon.GetPath(0).Length;
            for (int i = 0; i < vertexLength; i++)
            {
                Vector2 v1 = polygon.points[i];
                Vector2 v2 = polygon.points[(i + 1) % vertexLength];
                if (LineSegmentsCross(start, end, v1, v2))
                {
                    if (DistanceToSegment(start.x, start.y, v1.x, v1.y, v2.x, v2.y) < epsilon ||
                        DistanceToSegment(end.x, end.y, v1.x, v1.y, v2.x, v2.y) < epsilon)
                        return false;
                }

            }
        }

        Vector2 vector = start + end;
        Vector2 vector2 = new Vector2(vector.x / 2, vector.y / 2);

        bool inside = m_Polygons[0].OverlapPoint(vector2);

        for (int i = 0; i < m_Polygons.Count; i++)
        {
            if (m_Polygons[i].OverlapPoint(vector2))
                inside = false;
        }

        return inside;

    }
    //https://github.com/MicUurloon/AdventurePathfinding/blob/master/src/pathfinding/PolygonMap.hx
    private bool LineSegmentsCross(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
    {
        float denominator = ((b.x - a.x) * (d.y - c.y)) - ((b.y - a.y) * (d.x - c.x));

        if (Mathf.Round(denominator) == 0.0f)
        {
            return false;
        }

        float numerator1 = ((a.y - c.y) * (d.x - c.x)) - ((a.x - c.x) * (d.y - c.y));
        float numerator2 = ((a.y - c.y) * (b.x - a.x)) - ((a.x - c.x) * (b.y - a.y));

        if (Mathf.Round(numerator1) == 0.0f || Mathf.Round(numerator2) == 0.0f)
        {
            return false;
        }

        float r = numerator1 / denominator;
        float s = numerator2 / denominator;

        return (r > 0 && r < 1) && (s > 0 && s < 1);

        return true;
    }

    public float DistanceToSegment(float px, float py, float vx, float vy, float wx, float wy)
    {
        return Mathf.Sqrt(DistanceToSegmentSquared(px, py, vx, vy, wx, wy));
    }

    public float DistanceToSegmentSquared(float px, float py, float vx, float vy, float wx, float wy)
    {
        float l2 = DistanceSquared(vx, vy, wx, wy);
        if (Mathf.Round(l2) == 0.0f) return DistanceSquared(px, py, vx, vy);

        float t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2;

        if (t < 0) return DistanceSquared(px, py, vx, vy);
        if (t > 0) return DistanceSquared(px, py, wx, wy);
        return DistanceSquared(px, py, vx + t * (wx - vx), vy + t * (wy - vy));
    }

    public float DistanceSquared(float vx, float vy, float wx, float wy)
    {
        return (vx - wx) * (vx - wx) + (vy - wy) * (vy - wy);
    }


    //public void FindShortestPath()
  
}
