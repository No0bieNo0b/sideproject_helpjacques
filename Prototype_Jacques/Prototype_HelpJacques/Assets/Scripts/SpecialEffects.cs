﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class SpecialEffects : MonoBehaviour
{
    private static SpecialEffects instance;

    public ParticleSystem explosionEffect, pinchExplosionEffect;
    public GameObject trailsPrefab;


    void Awake()
    {
        instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        if (explosionEffect == null)
        {
            Debug.LogError("Missing Explosion Effect");
        }

        if (pinchExplosionEffect == null)
        {
            Debug.LogError("Missing pinchExplosion effect");
        }

        if (trailsPrefab == null)
        {
            Debug.LogError("Missing trails prefab");
        }
    }

    public static ParticleSystem MakeExplosion(Vector3 position)
    {
        if (instance == null)
        {
            Debug.LogError("There is no specialEffects script in the scene");
            return null;
        }

        ParticleSystem effect = Instantiate(instance.explosionEffect) as ParticleSystem;

        effect.transform.position = position;

        Destroy(effect.gameObject, effect.duration);

        return effect;
    }


    public static ParticleSystem MakePinchExplosion(Vector3 position)
    {
        if (instance == null)
        {
            Debug.LogError("There is no specialEffects script in the scene");
            return null;
        }

        ParticleSystem effect = Instantiate(instance.pinchExplosionEffect) as ParticleSystem;

        effect.transform.position = position;

        Destroy(effect.gameObject, effect.duration);

        return effect;
    }

    public static GameObject MakeTrail(Vector3 position)
    {
        if (instance == null)
        {
            Debug.LogError("There is no specialEffects script in the scene");
            return null;
        }

        GameObject trail = Instantiate(instance.trailsPrefab) as GameObject;

        trail.transform.position = position;

        return trail;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
