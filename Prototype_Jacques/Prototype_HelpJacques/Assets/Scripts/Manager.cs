﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Manager : MonoBehaviour
{
    // Start is called before the first frame update
    private static Manager m_Manager;

    void Start()
    {
        if (m_Manager == null)
            m_Manager = this;
    }

    public static Manager GetInstance()
    {
 
        return m_Manager;
    }

    public void SetActiveScene(string sceneName)
    {
       // SceneManager.LoadScene(sceneName);
       SceneManager.LoadScene(sceneName);
    }
    // Update is called once per frame
}
