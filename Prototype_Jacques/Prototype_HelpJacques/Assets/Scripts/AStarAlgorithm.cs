﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

public class AStarAlgorithm : MonoBehaviour
{

    private TraversablePath.Graph m_Graph = new TraversablePath.Graph();
    private List<TraversablePath.Graph.Edge> m_SPT = new List<TraversablePath.Graph.Edge>();
    private List<float> m_gScore = new List<float>();
    private List<float> m_fScore = new List<float>();
    private List<TraversablePath.Graph.Edge> m_SF = new List<TraversablePath.Graph.Edge>();
    private int m_Source;
    private int m_Target;

    private List<float> keys = new List<float>();
    private List<int> data = new List<int>();


// Start is called before the first frame update
void Start()
    {

    }

    public void New(TraversablePath.Graph graph, int source, int target)
    {
        m_Graph = graph;
        m_Source = source;
        m_Target = target;

        m_SPT = new List<TraversablePath.Graph.Edge>();
        m_gScore = new List<float>();
        m_fScore = new List<float>();
        m_SF = new List<TraversablePath.Graph.Edge>();

        for (int i = 0; i < m_Graph.GetNodes().Count; ++i)
        {
            m_gScore[i] = 0;
            m_fScore[i] = 0;
        }

        Search();
    }

    private void Search()
    {
        keys = m_fScore;
        data.Insert(data.Count, m_Source);

        while (data.Count != 0)
        {
            int NCN = data.Remove()
        }

    }

}
