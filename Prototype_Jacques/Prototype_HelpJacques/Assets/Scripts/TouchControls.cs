﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : MonoBehaviour
{
    private Dictionary<int, GameObject> trails = new Dictionary<int, GameObject>();
    private Touch pinchFinger1, pinchFinger2;
    private ParticleSystem pinchExplosion;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {

            //var finger1 = Input.GetTouch(0);
            //var finger2 = Input.GetTouch(1);

            //if (finger1.phase == TouchPhase.Began && finger2.phase == TouchPhase.Began)
            //{
            //    this.pinchFinger1 = finger1;
            //    this.pinchFinger2 = finger2;
            //}

            //if (finger1.phase == TouchPhase.Moved || finger2.phase == TouchPhase.Moved)
            //{
            //    float baseDistance = Vector2.Distance(this.pinchFinger1.position, this.pinchFinger2.position);
            //    float currentDistance = Vector2.Distance(finger1.position, finger2.position);

            //    float currentDistancePercent = currentDistance / baseDistance;

            //    if (pinchExplosion = null)
            //    {
            //        Vector3 finger1Postion = Camera.main.ScreenToWorldPoint(this.pinchFinger1.position);
            //        Vector3 finger2Postion = Camera.main.ScreenToWorldPoint(this.pinchFinger2.position);
            //    }

            //    pinchExplosion.transform.localScale = Vector3.one * (currentDistancePercent * 1.5f);

            //}
            //else
            // {
            //if (pinchExplosion != null)
            //{
            //    for (int i = 0; i < 10; i++)
            //    {
            //        var explosion = SpecialEffects.MakeExplosion(pinchExplosion.transform.position);
            //        explosion.transform.localScale = pinchExplosion.transform.localScale;
            //    }

            //    Destroy(pinchExplosion.gameObject);
            //}

            if (Input.touchCount > 0)
            {
                Debug.Log("a touch has occured!");
            }

            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);

                if (touch.phase == TouchPhase.Ended && touch.tapCount == 1)
                {
                    Vector3 position = Camera.main.ScreenToWorldPoint(touch.position);
                    SpecialEffects.MakeExplosion((position));
                }
                else
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (trails.ContainsKey(i) == false)
                        {
                            Vector3 position = Camera.main.ScreenToWorldPoint(touch.position);
                            position.z = 0;
                            GameObject trail = SpecialEffects.MakeTrail(position);

                            if (trail != null)
                            {
                                Debug.Log(trail);
                                trails.Add(i, trail);
                            }

                        }
                    }
                    else if (touch.phase == TouchPhase.Moved)
                    {
                        if (trails.ContainsKey(i))
                        {
                            GameObject trail = trails[i];
                            Vector3 position = Camera.main.ScreenToWorldPoint((touch.position));
                            position.z = 0;
                            trail.transform.position = position;
                        }
                    }
                    else if (touch.phase == TouchPhase.Ended)
                    {
                        if (trails.ContainsKey(i))
                        {
                            GameObject trail = trails[i];
                            Destroy(trail, trail.GetComponent<TrailRenderer>().time);
                            trails.Remove(i);
                        }
                    }
                }
            }
            // }

        }


    }
}
