﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Character : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] protected float m_CurrentSpeed;
    [SerializeField] protected float m_Speed;
    protected Vector2 m_Goal;

    [SerializeField] protected TraversablePath m_TraversablePath;

}
