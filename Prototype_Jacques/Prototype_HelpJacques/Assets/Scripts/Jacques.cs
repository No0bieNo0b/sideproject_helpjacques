﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Jacques : Character
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float elapsedSec = Time.deltaTime;

        if (Input.touchCount == 1)
        {
            m_CurrentSpeed = m_Speed;
            Touch touch = Input.GetTouch(0);
            m_Goal = Camera.main.ScreenToWorldPoint(touch.position);

            Vector2 direction;
            direction.x = m_Goal.x - transform.position.x;
            direction.y = m_Goal.y - transform.position.y;
            direction.Normalize();

            transform.Translate(direction.x * m_CurrentSpeed * elapsedSec, direction.y * m_CurrentSpeed * elapsedSec, 0.0f);
        }
        else
        {
            m_CurrentSpeed = 0.0f;
        }
    }

    //Vector2 GetClosentPathPoint(Vector2 target)
    //{
    //    float distanceTo;
    //    float minDistance = float.MaxValue;
    //    Vector2 closestPathPoint = new Vector2();
    //    Vector2 posJacques = new Vector2(transform.position.x, transform.position.y);
    //    foreach (Transform pathPoint in m_TraversablePath.GetPath())
    //    {  
    //        Vector2 posPathPoint = new Vector2(pathPoint.position.x, pathPoint.position.y);
    //        distanceTo = Vector2.Distance(posJacques, posPathPoint);

    //        if (distanceTo < minDistance)
    //        {
    //            minDistance = distanceTo;
    //            closestPathPoint = posPathPoint;
    //        }
    //    }
    //    return closestPathPoint;
    //}

}
